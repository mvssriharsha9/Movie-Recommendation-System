Flask==3.0.0
numpy==1.26.2
pandas==1.3.4
gunicorn==20.1.0
